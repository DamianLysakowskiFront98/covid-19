import React from "react";
import { Card, CardContent, Typography, Grid } from "@material-ui/core";
import styles from "./Cards.module.css";
import CountUp from "react-countup";
import cx from "classnames";
import imageCard from "../../img/image2.png";

const Cards = ({ data: { confirmed, recovered, deaths, lastUpdate } }) => {
  if (!confirmed) {
    return "Loading ...";
  }
  return (
    <div className={styles.container}>
      <Grid container spacing={1} justify="center">
        <Grid
          item
          component={Card}
          xs={12}
          md={10}
          className={cx(styles.card, styles.infected)}
        >
          <CardContent>
            <img className={styles.imageCard} src={imageCard} />
            <Typography color="textSecondary" gutterBottom>
              Zarażonych
            </Typography>
            <Typography variant="h5">
              <CountUp
                start={0}
                end={confirmed.value}
                duration={2.5}
                separator=","
              />
            </Typography>
            <Typography color="textSecondary">
              {new Date(lastUpdate).toDateString()}
            </Typography>
            <Typography variant="body2">
              Liczba zarażonych przez COVID-19
            </Typography>
          </CardContent>
        </Grid>
        <Grid
          item
          component={Card}
          xs={12}
          md={10}
          className={cx(styles.card, styles.recovered)}
        >
          <CardContent>
            <img className={styles.imageCard} src={imageCard} />
            <Typography color="textSecondary" gutterBottom>
              Wyleczonych
            </Typography>
            <Typography variant="h5">
              <CountUp
                start={0}
                end={recovered.value}
                duration={2.5}
                separator=","
              />
            </Typography>
            <Typography color="textSecondary">
              {new Date(lastUpdate).toDateString()}
            </Typography>
            <Typography variant="body2">
              Liczba wyleczonych z COVID-19
            </Typography>
          </CardContent>
        </Grid>
        <Grid
          item
          component={Card}
          xs={12}
          md={10}
          className={cx(styles.card, styles.deaths)}
        >
          <CardContent>
            <img className={styles.imageCard} src={imageCard} />
            <Typography color="textSecondary" gutterBottom>
              Zgonów
            </Typography>
            <Typography variant="h5">
              <CountUp
                start={0}
                end={deaths.value}
                duration={2.5}
                separator=","
              />
            </Typography>
            <Typography color="textSecondary">
              {new Date(lastUpdate).toDateString()}
            </Typography>{" "}
            <Typography variant="body2">
              Liczba zgonów spowodowanych COVID-19
            </Typography>
          </CardContent>
        </Grid>
      </Grid>
    </div>
  );
};

export default Cards;
